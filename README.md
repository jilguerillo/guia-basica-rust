# Rust Basic Guide

### Rust basics as a guide with comments for further information

<div>
    <ul>
        <li>Basic data</li>
        <li>Types</li>
        <li>Structures</li>
        <li>Functions</li>
        <li>References</li>
        <li>Memory</li>
        <li>Type of statements</li>
        <li>Etc....</li>
    </ul>
</div>
