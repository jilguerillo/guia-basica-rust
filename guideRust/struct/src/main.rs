#[derive(Debug)]
struct Persona { 
    nombre: String,
    edad: u8,
    altura: u8,
    email: String,
}

//generics esla manera de crear tipos abstractos, reduce el tiempo de ejecución
#[derive(Debug)]
struct Punto<T, U> {
    x: T,
    y: U,
}

//#[derive(Debug)]
//struct Coordenada(i32, i32); //struct tipo tupla

fn main() {
    let mut juan = Persona {
        nombre: String::from("Juan"),
        edad: 25,
        altura: 189,
        email: "juan@email.com".to_string(),
    };
    juan.nombre = "Joan".to_string();

    let mut ramon = Persona {
        nombre: "Ramon".to_string(),
        email: "juan@email.com".to_string(),
        ..juan
    };
    println!("struct juan  {:?}", juan);
    println!("struct ramon  {:?}", ramon);

    //let punto = Coordenada(3, -34);
    let punto = Punto {x: 1.2, y: -40};
    println!("{:?}", punto);
}
