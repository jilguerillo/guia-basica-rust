// Tipo de dato compuesto -> TUPLAS, arreglo de varios datos de distinto tipo
// Tiene tamaño fijo

fn main {
    let t = (3, 23, false, 3.33, "tupla");
    let t2 = (3, 23, flase, t);
    let tup: (i32, f32, &str) = (123, 2.3, "Tupla");     
    let fnT = retorna_tupla(2, 4);
    println!("{:?}", t); // {:?} imprime estructura
    
    let (x, y, z) = tup;
    println!("{}", x); // Destructuración

    let x2 = tup.1; // Obtiene valor con indice 1 de la tupla

    let mut tupMut: (i32, f32, &str) = (123, 2.3, "Tupla");     
    tupMut.1 = 4.4; // Cambia un valor de la tupla. Hay que respetar los valores

    fn retorna_tupla(x: u8, y: u8) -> (u8, u8) {
        (x, y)
    }
}