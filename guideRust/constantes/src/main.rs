fn main() {
    // Por convencion las constantes son en mayuscula y si son más de dos palabras se
    // separan por un guion bajo
    // Se debe inicializar el tipo de constante
    const  MI_CONSTANTE: u32 = 100_000; // -->  100,000 DE ESTA FORMA HAY ERROR, pero no 100000 
    sombreadoVariables();
    println!("{}", MI_CONSTANTE);
    // son siempre inmutable
    // en Rust hay constantes en las librerias estandar
}

fn sombreadoVariables(){
    let x = 10; // valor inicial
    println!("{}", x);
    let x = "10 --> en string"; //Se sombrea variable x con otro tipo de datos. No funciona con mut 
    println!("{}", x);


}