// Char tipo primitivo que representa 1 solo caracter de 4 bytes
// Se ocupan las comillas simples '', las comillas "" son para string de más de un carácter

fn main() {
    let v = vec!['h', 'e', 'l', 'o'];
    
    let a = char::from(65);
    println!("{}", a);
}
