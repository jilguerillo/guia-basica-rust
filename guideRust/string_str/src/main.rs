fn main() {
    let str:&str = "holas";
    let j = "仏教";
    let elem = &j.chars().nth(0);
    
    if let Some(e) = elem {
        println!("elemento {}", e);
    }

    //String con más metodos y más manipulables
    let string = String::from("Holi");
    let mut string2 = String::new();

    

    let mut string3 = String::from(" Holo");
    string3.push('教');

    string2.push_str("Chau");

    println!("{} {} {}", string, string2, string3); //string es un vector


    let mut parametroStr = String::from("Soy un parámetro"); // Se almacena en memoría Hit
    saludos(&parametroStr);
}

fn saludos(saludo: &str) {
    // Diferencias entre declaraciones String
    let mut saludo = "Hola Mundillo"; // Limitada en métodos
    let mut saludo1 = String::from("Hola Mundillo"); // Se almacena en memoría Heat

    saludo1.capacity(); // Método devuelve la capacidad del string, solo con String::, no con str
    saludo1.push_str(" Desde Rust");  // Concatena un String , solo con String::, no con str
    println!("{}", saludo1);
}
