//permite retornar un print, puede ser utilizado en distintos lugares, bd, etc
// todo lo aplicable en format se puede en println
fn main() {
    let papa = "Juan";
    let mama = "Juana";
    let hija = "Kalila";
    let edad_Kalila = 3;
    let texto = format!("{} y {} tienen una hija llamada {kalila}, {kalila}
    tiene {} años",
    mama,
    papa,
    edad_Kalila,
    kalila = hija, //crear tag para representar en el print
);
    println!("{}", texto);
}
