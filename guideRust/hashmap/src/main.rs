use std::collections::HashMap;


//hashmap no es coleccion de elemento para ordenar, es útil para obtener elementos
//especificos

fn main() {
    let mut num = HashMap::new();
    let diez = String::from("diez");
    let veinte = String::from("veinte");
    let treinta = String::from("treinta");
    let x = 10;
    let y = 20;
    let z = 30;
    let actualizado = String::from("Diez");
    num.insert(x, &diez);
    num.insert(y, &veinte);

    let q = num.entry(z).or_insert(&treinta); // si valor z no esta en num (vacante), le paso treinta en z

    println!("{:?}", q); //devuelve Enum y valor ocupado o vacante

    //num.insert(x, &actualizado); reemplazara el valor de x diez por el Diez de actualizado
    

    //recorrer hasmap
    for (k, v) in &num { //usar referencia de num, porque le pasa valor a for y no se podrá imprimir
        println!("{} => {}", k, v);
    }
    let elem = num.get(&x);   // key de parametro debe ser referencia - retorna -> Option<&V> osea un None o Some<T>
    match elem {
        Some(i) => println!("valor: {}", i),
        _ => println!("No encuentro valor"),
    }
    println!("num {:?}", num);
    //println!("{:?}", elem);
}
