use std::rc::Rc;

enum Lista {
    Nodo(i32, Rc<Lista>),
    None,
}
use Lista::*;

fn main() {
    let lista = Rc::new (Nodo(
        2, 
        Rc:: new (Nodo(
            4, 
            Rc:: new (Nodo(
                7, Rc:: new (None)))))));

    println!("contador de referencias lista: {}", Rc::strong_count(&lista));
    //let listaB = Nodo(45, Box::new(lista)); // valor de lista fue removido a listaB con Box, con Rc no
    let listaB = Nodo(45, Rc::clone(&lista)); //clona referencia lista con Rc
    {
        let listaScope = Nodo(45, Rc::clone(&lista));
        println!("contador de referencias lista: {}", Rc::strong_count(&lista));    
    }// en scope se elimina la variable listaScope y valor
    let listaC = Nodo(45, Rc::clone(&lista));
    println!("contador de referencias lista: {}", Rc::strong_count(&lista));
}
