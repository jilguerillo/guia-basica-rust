use std::collections::HashSet;

//hashset conjunto no pueden haber tipos repetidos
//hashset no ordena elementos
fn main() {
    let mut c1 = HashSet::new();
    c1.insert('a');
    c1.insert('b');
    c1.insert('c');
    c1.insert('d');
    c1.insert('e');
    c1.insert('g');
    c1.insert('h');
    println!("Hashset : , {:?}", c1); 

    //metodo contains
    let letra = 'a';
    println!("{:?} contiene {} ? {}", c1, letra, c1.contains(&letra)); //crea un conjunto de letras de la a a la z

    //metodo collect y is_subset
    let c2: HashSet<_> = ('a'..='z').collect();
    println!(
        "{:?} es subconjunto de  {:?} ? {}", c1, c2, c1.is_subset(&c2),
    );

}
