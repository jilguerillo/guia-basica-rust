fn main() {
    let x = 3;
    let y = 7;
    let a = f(&x, &y);
    println!("{}", a);

}


#[allow(unused_variables)]
fn f<'a>(param1: &'a i32, param2: &'a i32) -> &'a i32
{
    if param1 < param2{
        param1
    }else
    {
        param2
    }
}
