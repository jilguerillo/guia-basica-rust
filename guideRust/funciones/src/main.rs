// Función principal donde se ejecuta el script es Main
fn main() {
    funcion_uno(); // Llamada función sin parámetros
    funcion_con_parametros(12, 233);
    let x = funcion_retorna_valor();
    println!("{}", x)
}

// Por convención se ocupa _ para separar las palabras
fn funcion_uno() {
    println!("Función uno") // Para funciones como estas de una sola línea no es 
    // necesario terminar con ; punto y coma, le estamos indicando el retorno de un valor 
}

// Función con parametros. Es necesario indicar el tipo de datos
fn funcion_con_parametros(x : i16, y : i16) {
    println!("Función con parámetros, valor x {} , valor y {}", x, y)
}

// Para retorno de valor se necesita indicar el tipo de retorno
fn funcion_retorna_valor() -> i32{
    println!("Función retorna valor");
    1000   // sin ; no es necesario usar return. 
}