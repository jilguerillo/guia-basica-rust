// Utiliza ownership permite cuidar la seguridad en el uso de la memoria, como memoryLeak, etc.
// Se ejecuta con el programa, pero no lo hace más lento
// Relacionado con la memoria stack y heap

// Stack : Es aplicado a la structura de datos y utilizado en Rust. Es instantanea pero usa
// tamaños conocidos y mientras no cambie la capacidad del valor

// Heap : busca donde almacenar la información o datos, es un poco más lento pero puede aunmentas
// la capacidad de los valores mientras se ejecuta el programa

// Reglas ownership
// 1.- Cada variable contiene o  es un owner
// 2.- Solo un owner por valor
// 3.- El owner es eliminado al salir del Scope


fn main(){
    let s1 = String::from("S1");
    toma_ownership(s1);  // Se toma el s1 y no puede seguir siendo utilizado como por ejemplo
    /// para un printl!, para seguir con la variable se debe pasar una referencia de la variable 
    /// toma_ownership(&s1);
    s1Clone();
}


fn scope1() {
    let scope1 = String::from("ownership"); // scope1 entra al scope
                                        // scope1 es el ownership
    println!("{}", scope1);    
}   // se elimina scope1

fn s1Clone(){
    //numeros primitivos se guardan en stack/pila y una parte de los string tambien y la otra en el heap 
    let scope1 = String::from("ownership"); // owner scope1
    let s2 = scope1.clone(); 
    println!("{}", s2);
}

fn toma_ownership(cadena_texto: String) {
    println!("{}", cadena_texto);
}