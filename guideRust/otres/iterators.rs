fn main {
	let n = 10;
	for i in 0..n {
		println!("{}", i);
	}
	// trait Iterator {
	// 	type Item;
	// 	fn next(&mut self) -> Option<Self::Item>;
	// 	// muchos otros métodos por defecto
	// }

    let v = vec![1,2,3];
    let mut iter = v.iter();
    assert_eq!(iter.next(), Some(&1));
    assert_eq!(iter.next(), Some(&2));
    assert_eq!(iter.next(), Some(&3));
    assert_eq!(iter.next(), None);
    
	// map adaptor
    let v = vec![1,3,5,6];
    let x: Vec<_> = v.iter().map(|n| n * 2).collect();
    println!("{:?}", x);    
    
    // filter adaptor
    let animales = " camellos vacas gatos perros elefantes     ";
    let v: Vec<&str> = animales
        .split_whitespace()
        .filter(|a| a != &"gatos")
        .collect();
    println!("{:?}", v);
    
    // consumers min, max, sum
    let v = vec![1,3,5,6];
    let total: u8 = v.iter().sum();
    println!("{:?}", total);
    let v = vec![1,3,5,6];
    let min = v.iter().min();
    println!("{:?}", min);
    let v = vec![1,3,5,6];
    let max = v.iter().max();
    println!("{:?}", max);
}
