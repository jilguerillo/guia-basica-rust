trait Mostrar {
    fn mostrar(&self) -> String;
}

impl Mostrar for i32 {
    fn mostrar(&self) -> String {
        format!("entero cuatro bytes: {}", self)
    }
}

impl Mostrar for f64 {
    fn mostrar(&self) -> String {
        format!("decimal ocho bytes: {}", self)
    }
}

trait Sonido {
    fn hace(&self) -> String;
}

struct Gato {}
struct Perro {}
struct Tractor {}

impl Sonido for Gato {
    fn hace(&self) -> String {
        "Miau".to_string()
    }
}

impl Sonido for Perro {
    fn hace(&self) -> String {
        "Guau".to_string()
    }
}

impl Sonido for Tractor {
    fn hace(&self) -> String {
        "Brrrrrrr".to_string()
    }
}

impl Sonido for i32 {
    fn hace(&self) -> String {
        match &self {
            1 => "U-ENE-O".to_string(),
            2 => "DE-O-ESE".to_string(),
            3 => "TE-ERRE-E-ESE".to_string(),
            _ => r#"¯\_(ツ)_/¯"#.to_string(),
        }
    }
}

fn main() {
    let x = 44;
    let uno = 1;
    let tres = 3;
    let y = 2.7;
    let v: Vec<&Mostrar> = vec![&x, &y];
    for d in v.iter() {
        println!("muestra {} ", d.mostrar());
    }
    let gato = Gato {};
    let perro = Perro {};
    let tractor = Tractor {};
    println!("El perro hace {}", perro.hace());
    println!("El gato hace {}", gato.hace());
    println!("El tractor hace {}", tractor.hace());
    println!("El uno hace {}", uno.hace());
    println!("El tres hace {}", tres.hace());
    println!("El x hace {}", x.hace());
}

