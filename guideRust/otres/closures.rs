fn main() {
    let mut dict = std::collections::HashMap::new();
    dict.insert(1, "uno");
    dict.insert(2, "dos");
    dict.insert(3, "tres");
    let cl = || {
        for (k, v) in dict {
            println!("k: {} v: {}", k, v);
        }
    };
    cl();
    cl();
    
    let mut i = 1;
    let mut inc = || {// FnMut
    	i += 1;
    	i
    };
    println!("{}", inc());
    println!("{}", inc());
    println!("{}", inc());
    
	let mut x = vec![1,2,3];
	let mut y = || x.pop(); // FnMut

	println!("{:?}" , y());
	println!("{:?}" , x);
}
