trait Animal {
    fn habla(&self);
}

struct Gato {}
struct Perro {}

impl Animal for Gato {
    fn habla(&self) {
        println!("Miau");
    }
}

impl Animal for Perro {
    fn habla(&self) {
        println!("Guau");
    }
}

fn main() {
    let g = Gato{};
    let p = Perro{};
    emite_sonido(&g);
    emite_sonido(&p);
}

// Box<dyn Animal>
// Rc<dyn Animal>
fn emite_sonido(a: &dyn Animal) {
    a.habla();
}

fn get_animal() -> Box<dyn Animal> {
    if true {
        return Box::new(Perro{});
    } else {
        return Box::new(Gato{});
    }
}

