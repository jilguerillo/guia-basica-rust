trait Animal {
    fn habla(&self);
}

struct Gato {}
struct Perro {}

impl Animal for Gato {
    fn habla(&self) {
        println!("Miau");
    }
}

impl Animal for Perro {
    fn habla(&self) {
        println!("Guau");
    }
}

fn main() {
    let g = Gato{};
    let p = Perro{};
    emite_sonido(g);
    emite_sonido(p);
}

// monomorfización
fn emite_sonido<J: Animal>(a: J) {
    a.habla();
}

