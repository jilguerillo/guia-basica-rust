fn main() {
    let mut a = [34, 56, 12, 99, 82, 300];
    let slice = &a[..4];
    let get = get_slice(&a);
    println!("slice 1: {:?}", slice);
    println!("slice 2: {:?}", get);
    let x = a.len()-3;
    modifica(&mut a[x..]);
    println!("slice modificado: {:?}", a);
}

fn get_slice(s: &[i32]) -> &[i32] {
    &s[2..5]
}

fn modifica(s: &mut[i32]) {
    s[0] = 0;
}

