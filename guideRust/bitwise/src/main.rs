fn main() {
    const COIN: i64 = 100000000; // satoshi
    let n_best_height = 690000;
    let mut n_subsidy = 50 * COIN;
    n_subsidy >>= n_best_height / 210000;
    println!("{}", n_subsidy);
}
