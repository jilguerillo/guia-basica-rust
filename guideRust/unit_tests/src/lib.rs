

pub mod saludos {
    pub mod esp {
        pub fn hola() -> String {
            "hola".to_string()
        }
        pub fn chao() -> String {
            "chao".to_string()
        }
    }
    pub mod frn {
        pub fn hola() -> String {
            "salut".to_string()
        }
        pub fn chao() -> String {
            "au revoir".to_string()
        }
    }
}

