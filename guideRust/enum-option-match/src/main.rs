#[derive(Debug)]
enum Direccion {
    Arriba,
    Abajo,
    Izquierda,
    Derecha,
    Noreste,
}

#[derive(Debug)]
enum Logo {
    Descripcion(String),
    Color(u8, u8, u8),
    Archivo(File),
}

//#[derive(Debug)]
//enum Option<T> {
//    Some(T),
//    None,
//}

#[allow(dead_code)]
enum SimpleEnum<T>{
    OptionA(T),
    OptionB(T),
    OptionC,
}


#[derive(Debug)]
struct File {}

#[derive(Debug)]
struct Persona { 
    nombre: String,
    edad: u8,
    altura: u8,
    //email: Option<String>,
    email: Option<String>,
}



#[allow(unused_variables)]
fn main() {
    let mut pedro = Persona {
        nombre: "Pedro".to_string(),
        edad: 23,
        altura: 170,
        email: None,
    };
    pedro.email = Some("pedro@gmail.come".to_string());
    println!("El email es: {}", pedro.email.unwrap_or("NO HAY".to_string()));
    //println!("{:?}", match pedro.email {
    //    Some(x) => x,
    //    _ => "No es ningún tipo".to_string()
    //});

    let v = vec![0, 3, 34, 53, 53];
    let valor_alto = v.get(3); //obteniendo valor
    match valor_alto {
        Some(x) => println!("el numero es {}", x),
        None => println!("fuera de ranfo"),
    }

    ////////////////////////////////////////////////////////
    let x = SimpleEnum::OptionA(60);
    let y = SimpleEnum::OptionB("TEXT X".to_string());
}




fn enum2() {
    let color = Logo::Color(23, 23, 99);
    let descripcion = Logo::Descripcion("mi logo".to_string());
    let v = vec![color, descripcion]; //vector
    println!("{:?}", v) //mismo tipo logo con distintos valores
}

fn enum1() {
    let arriba = Direccion::Noreste;
    match arriba {
        Direccion::Arriba => println!("Arriba"),
        Direccion::Abajo => println!("Arriba"),
        Direccion::Izquierda => println!("Arriba"),
        Direccion::Derecha => println!("Arriba"),
        _ => println! ("No especificado en las opciones"),
    }
}
