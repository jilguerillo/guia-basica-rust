use std::mem;
use std::f64::consts; 


fn main() {
    let x: i8 = 1; // i = integer 8 con signo ... = 8 bits...i8, i16, i32, i64 sirve para negativos y positivos
                    // u8 = entero sin signo 8 bits.....u8, u16, u32, u64 solo positivos
                    //isize, usize -> arch depende de la arquitectura de la computadora
                    //i = isigned, u = insigned
                    // para las operaciones tienen que tener la misma extemsion. ejemplo: f32 + f32, i64 - i64 
    let y: usize = 10000000000000000;
    let size = mem::size_of_val(&y);
    println!("x = {} y = {} OS {} bits", x, y, size*8); // ! = macro ..... {} = placeholder

    //flotantes f32, f64
    let f: f64 = 34.83;
    println!("f = {}", f);

    //hexadecimal, binario, octal
    let hexadecimal = 0x20; //guardado como entero 32
    let octal = 0o10; //guardado como entero 8
    let binario = 0b1010101010;
    println!("hexadecimal = {} , octal = {} , binario = {}", hexadecimal, octal, binario);
    println!("Tamaño del binario = {}", mem::size_of_val(&binario));

    //CHAR
    let c: char = 'c';
    println!("c = {} y tamaño = {}", c, mem::size_of_val(&c));

    //Booleano
    let b: bool = true;
    println!("b = {} y tamaño = {}", b, mem::size_of_val(&b));

    //separadores
    let s: usize = 10_000_000_000_000_000;
    println!("s = {} y tamaño = {}", s, mem::size_of_val(&s));

    // Rust asigna a todas las variables inmutabilidad, hay que asignarles
    // mut para mutarlas
    
    println!("*******************************************************");

    let a: i32 = 123;
    let mut b = 100;
    b = b+1;

    println!("{}, {}", a, b );

    // const por convencion son en mayuscula
    // son siempre inmutable
    // en Rust hay constantes en las librerias estandar

    println!("{}", consts::PI);

    // Parceo de datos
    let parceo : u16 = "9999".parse().expect("No es número");
    println!("----> Parceo : {}", parceo);

    let xxx : isize = 999999999999999999; 

}
